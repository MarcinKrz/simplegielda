/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin;

import java.io.Serializable;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * szablon w�tku pozwalaj�cy na zako�czenie jego pracy , oraz wielokrotne
 * przerywanie i wznawianie jego pracy
 *
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InfinitLoopThread extends Thread implements Serializable {

    private volatile boolean running = true;
    private volatile boolean pause = false;
    private Runnable runnable;

    public InfinitLoopThread(Runnable runnable) {
        this.runnable = runnable;
    }

    @Override
    public void run() {
        while (running) {

            runnable.run();

            while (pause) {
                GieldaApp.sleep(500);
            }
            GieldaApp.sleep(2000);

        }
    }

    /**
     * metoda powoduj�ca zako�czenie pracy w�tku
     */
    public void stopRunning() {
        this.running = false;
    }

    /**
     * metoda zatrzymuj�ca w�tkek
     */
    public void pauseRunning() {
        this.pause = true;
    }

    /**
     * wznowienie w�tku
     */
    public void resumeRunning() {
        this.pause = false;

    }
}
