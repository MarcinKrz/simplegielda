/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import pl.krzemarcin.gielda.DzialaczRynkowy;

/**
 * klasa odpowiadaj�ca za rysowanie wykresow warto�ci kurs�w aktyw
 *
 * @author Marcin
 */
public class OknoPorownaniaKursow extends javax.swing.JFrame {

    public OknoPorownaniaKursow() {
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OknoPorownaniaKursow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OknoPorownaniaKursow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OknoPorownaniaKursow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OknoPorownaniaKursow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            }
        });
    }

    private XYDataset createDataset(List<DzialaczRynkowy> wybrane) {
        final XYSeriesCollection dataset = new XYSeriesCollection();

        final Date najstarszaData = wybrane.stream()
                .flatMap(dzialacz -> dzialacz.getHistoriaKursu().entrySet().stream())
                .map(entrySet -> entrySet.getKey())
                .sorted()
                .findFirst()
                .orElseGet(() -> new Date());

        for (DzialaczRynkowy dzialaczRynkowy : new ArrayList<>(wybrane)) {
            Map<Date, Float> historiaKursu = new HashMap<>(dzialaczRynkowy.getHistoriaKursu());

            Set<Entry<Date, Float>> setHistoriaKursu = new HashSet<>(historiaKursu.entrySet());
            Float najstarszyKurs = setHistoriaKursu.stream()
                    .sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
                    .findFirst()
                    .map(entry -> entry.getValue())
                    .orElse(1f);

            XYSeries series = new XYSeries(dzialaczRynkowy.getNazwa(), true);
            for (Entry<Date, Float> entry : setHistoriaKursu) {
                float x = 1.0f * (entry.getKey().getTime() - najstarszaData.getTime()) / (60 * 1000);
                float y = 100.0f * (entry.getValue() - najstarszyKurs) / najstarszyKurs;
                series.add(x, y);
            }
            dataset.addSeries(series);

        }

        return dataset;

    }

    private XYDataset createDataset_old(List<DzialaczRynkowy> wybrane) {
        final XYSeriesCollection dataset = new XYSeriesCollection();

        final Date najstarszaData = wybrane.stream()
                .flatMap(dzialacz -> dzialacz.getHistoriaKursu().entrySet().stream())
                .map(entrySet -> entrySet.getKey())
                .sorted()
                .findFirst()
                .orElseGet(() -> new Date());

        for (DzialaczRynkowy dzialaczRynkowy : new ArrayList<>(wybrane)) {
            Map<Date, Float> historiaKursu = new HashMap<>(dzialaczRynkowy.getHistoriaKursu());
            Set<Entry<Date, Float>> setHistoriaKursu = new HashSet<>(historiaKursu.entrySet());
            Float najstarszyKurs = setHistoriaKursu.stream()
                    .sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
                    .findFirst()
                    .map(entry -> entry.getValue())
                    .orElse(1f);
            XYSeries series = new XYSeries(dzialaczRynkowy.getNazwa(), true);
            for (Entry<Date, Float> entry : setHistoriaKursu) {
                float x = 1.0f * (entry.getKey().getTime() - najstarszaData.getTime()) / (60 * 1000);
                series.add(x, entry.getValue());
            }
            dataset.addSeries(series);

        }

        return dataset;

    }

    private JFreeChart createChart(final XYDataset dataset) {

        final JFreeChart chart = ChartFactory.createXYLineChart(
                "Wykres Kursow Aktyw", // chart title
                "Czas", // x axis label
                "Wartosc Kursu", // y axis label
                dataset, // data
                PlotOrientation.VERTICAL,
                true, // include legend
                true, // tooltips
                false // urls
        );
        chart.setBackgroundPaint(Color.white);

        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);

        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        return chart;

    }

    private JFreeChart createWykresProcentowo(final XYDataset dataset) {

        final JFreeChart chart = ChartFactory.createXYLineChart(
                "Wykres Kursow Aktyw", // chart title
                "Czas", // x axis label
                "Procentowa zmiana Kursu", // y axis label
                dataset, // data
                PlotOrientation.VERTICAL,
                true, // include legend
                true, // tooltips
                false // urls
        );
        chart.setBackgroundPaint(Color.white);

        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(1, true);
        plot.setRenderer(renderer);

        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        return chart;

    }

    public void wykresProcentowy(List<DzialaczRynkowy> wybrane) {
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        final XYDataset dataset = createDataset(wybrane);
        final JFreeChart chart = createWykresProcentowo(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }

    public void wykresWartosci(List<DzialaczRynkowy> wybrane) {
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        final XYDataset dataset = createDataset_old(wybrane);
        final JFreeChart chart = createChart(dataset);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
