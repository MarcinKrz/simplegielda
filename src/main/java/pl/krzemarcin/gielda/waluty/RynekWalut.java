package pl.krzemarcin.gielda.waluty;

import pl.krzemarcin.gielda.Rynek;

/**
 * klasa implementująca klase Rynek , reprezentująca Rynek Surowców
 *
 * @author Marcin
 */
public class RynekWalut extends Rynek<Waluta> {

    public RynekWalut(String nazwa, Float marza) {
        super(nazwa, marza);
    }

}
