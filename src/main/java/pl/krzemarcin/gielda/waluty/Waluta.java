package pl.krzemarcin.gielda.waluty;

import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.Wlasciciel;

/**
 * klasa reprezentuj�ca pewn� ilo�� surowca wystawionego do obiegu
 *
 * @author Marcin
 */
public class Waluta extends Aktywa<Waluta> {

    public Waluta(KursWaluty tworca, Wlasciciel wlasciciel, int ilosc, String nazwa) {
        super(tworca, wlasciciel, ilosc, nazwa, false);
    }

    /**
     * metoda zwracaj�ca ��czn� cenewaluty
     *
     * @return
     */
    @Override
    public float getCenaJd() {
        return this.getTworca().getKurs();
    }
}
