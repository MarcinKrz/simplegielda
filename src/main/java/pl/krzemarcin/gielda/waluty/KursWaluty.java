package pl.krzemarcin.gielda.waluty;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.Rynek;

/**
 * mplementacja interfejsu DziałaczRynkowy służąca do zachowywania historii ceny
 * oraz historii ofert sprzedaży,klasa odpowiedzialna jest za dostarczanie
 * nowych walut do obiegu
 *
 * @author Marcin
 */
@Getter
public class KursWaluty implements DzialaczRynkowy<Waluta>, Serializable {

    private String nazwa;
    private String listaKrajow;
    private Float kurs;
    private Float minKurs;
    private Float maxKurs;
    private Rynek<Waluta> rynek;
    private Map<Date, Float> historiaKursu = new HashMap<>();
    private Map<Date, Integer> historiaSprzedazy = new HashMap<>();
    private Map<Date, Integer> historiaOfertSprzedazy = new HashMap<>();

    public KursWaluty(String nazwa, String listaKrajow, Float kurs, Rynek<Waluta> rynek) {
        this.nazwa = nazwa;
        this.listaKrajow = listaKrajow;
        this.kurs = kurs;
        this.rynek = rynek;
        this.minKurs = kurs;
        this.maxKurs = kurs;
        this.historiaKursu.put(new Date(), kurs);
    }

    /**
     * metoda ustawiająca nową wartość kursu
     *
     * @param kurs
     */
    @Override
    public void setKurs(Float kurs) {
        this.kurs = kurs;
        historiaKursu.put(new Date(), kurs);
        if (this.kurs < minKurs) {
            this.minKurs = kurs;
        }
        if (this.kurs > maxKurs) {
            this.maxKurs = kurs;
        }
    }

    /**
     * metoda rejestrująca w historii kupno waluty
     *
     * @param ilosc
     */
    @Override
    public void zarejestrujSprzedaz(Integer ilosc) {
        historiaSprzedazy.put(new Date(), ilosc);
    }

    /**
     * metoda odpowiadająca za wytworzenie waluty
     *
     * @param ilosc wytworznej waluty
     * @return wytworzona waluta
     */
    @Override
    public Waluta wytworzAktywa(Integer ilosc) {
        Waluta waluta = new Waluta(this, null, ilosc, this.nazwa);
        zarejestrujOferteSprzedazy(ilosc);
        return waluta;
    }

    /**
     * metoda odpowiadająca za zarejestrowanie w historii oferty sprzedaży
     * waluty
     *
     * @param ilosc ilość waluty wystawionej na sprzedaz
     */
    @Override
    public void zarejestrujOferteSprzedazy(Integer ilosc) {
        historiaOfertSprzedazy.put(new Date(), ilosc);
    }

    /**
     * metoda zwraca rynek na którym jest notowana waluta
     *
     * @return
     */
    @Override
    public Rynek<Waluta> getRynek() {
        return rynek;
    }

    @Override
    public String toString() {
        return getNazwa();
    }

}
