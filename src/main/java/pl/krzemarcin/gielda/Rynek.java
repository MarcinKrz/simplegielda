package pl.krzemarcin.gielda;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import lombok.Getter;
import pl.krzemarcin.gielda.wyjatek.NiewystarczajacyBudzet;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * klasa abstrakcyjna odpowiadaj�ca ogolnej idei rynku , a wi�c operacji kupna ,
 * wystawiania ofert przez w�a�cicieli itp
 *
 * @author Marcin
 * @param <T> rodzaj Aktywa jakie mo�e zosta� wystawione na rynku
 */
//@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class Rynek<T extends Aktywa> implements Serializable {

    @Getter
    private volatile Map<DzialaczRynkowy<T>, Set<T>> aktywaRynku = new HashMap<>();

    /**
     *
     * @param nazwa nazwa Rynku
     * @param marza marza pobierana przez Rynek
     */
    public Rynek(String nazwa, Float marza) {
        this.nazwa = nazwa;
        this.marza = marza;
    }

    @Getter
    private String nazwa;

    @Getter
    private Float marza;

    /**
     * metoda do wystawiania oferty sprzedazy na rynku
     *
     * @param aktywo aktywo ,kt�re ma zosta� wystawione na sprzedaz na danym
     * rynku
     */
    public synchronized void wystawOferte(T aktywo) {
        if (!aktywaRynku.containsKey(aktywo.getTworca())) {
            aktywaRynku.put(aktywo.getTworca(), new HashSet<>());
        }
        aktywaRynku.get(aktywo.getTworca()).add(aktywo);
        aktywo.getTworca().zarejestrujOferteSprzedazy(aktywo.getIlosc());
        aktywo.setJestNaRynku(true);
    }

    /**
     * metoda zwraca zbi�r aktyw notowanych an rynku
     *
     * @return
     */
    public synchronized Set<T> aktywaNaRynku() {
        return aktywaRynku.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    /**
     * metoda kupna aktywa na rynku
     *
     * @param kupujacy W�a�ciciel kt�ry chce kupi� aktywo na rynku
     * @param dzialacz typ kupowanego aktywa
     * @param ilosc ilosc aktywa do kupna
     * @throws NiewystarczajacyBudzet wyj�tek rzucany gdy kupujacy posiada za
     * ma�o pieniedzy
     */
    public synchronized void kup(Wlasciciel kupujacy, DzialaczRynkowy<T> dzialacz, Integer ilosc) throws NiewystarczajacyBudzet {

        final Set<T> ofertySprzedazyAktywa = getAktywaRynku().get(dzialacz);
        final T wybraneAktywo = ofertySprzedazyAktywa.stream()
                .filter(oferta -> ilosc.equals(oferta.getIlosc()))
                .findAny()
                .orElse(null);
        if (wybraneAktywo == null) {
            T wytworzone = dzialacz.wytworzAktywa(ilosc);
            final float cena = wytworzone.getCenaCalkowita();
            try {
                kupujacy.pobierzPieniadze(cena);
                wytworzone.setWlasciciel(kupujacy);
                kupujacy.dodajAktywo(wytworzone);
                dzialacz.zarejestrujSprzedaz(ilosc);
            } catch (NiewystarczajacyBudzet a) {
            }

        } else {
            final T wycofaneAktywo = wycofaj(wybraneAktywo);

            final Wlasciciel sprzedajacy = wybraneAktywo.getWlasciciel();
            if (wycofaneAktywo != null) {
                final float cenaAktywa = wybraneAktywo.getCenaCalkowita();
                final float cenaAktywaMinusMarza = cenaAktywa * (1 - (getMarza() / 100));
                try {
                    kupujacy.pobierzPieniadze(cenaAktywa);
                    sprzedajacy.dokonajSprzedazy(wybraneAktywo, cenaAktywaMinusMarza);
                    kupujacy.dodajAktywo(wybraneAktywo);
                    wybraneAktywo.setJestNaRynku(false);
                    dzialacz.zarejestrujSprzedaz(wybraneAktywo.getIlosc());

                } catch (NiewystarczajacyBudzet ex) {
                    wstawPonownie(wybraneAktywo);
                    throw ex;
                }
            }
        }
    }

    /**
     * metoda wycofania aktywa z rynku
     *
     * @param aktywo aktywo do wycofania
     * @return wycofane aktywo
     */
    public synchronized T wycofaj(T aktywo) {
        Set<T> aktywa = aktywaRynku.get(aktywo.getTworca());
        if (aktywa.remove(aktywo)) {
            return aktywo;
        } else {
            return null;
        }
    }

    /**
     * metoda implementuj�ca ponowne wstawienie aktywa na rynek
     *
     * @param aktywo
     */
    public synchronized void wstawPonownie(T aktywo) {
        Set<T> aktywa = aktywaRynku.get(aktywo.getTworca());
        aktywa.add(aktywo);
    }

    @Override
    public String toString() {
        return getNazwa();
    }

    /**
     * metoda rejestracji nowego Aktywa na Rynku
     *
     * @param nowyRodzajAktywa
     */
    public synchronized void zarejestrujAktywoNaRynku(DzialaczRynkowy<T> nowyRodzajAktywa) {
        aktywaRynku.put(nowyRodzajAktywa, new HashSet<>());
    }
}
