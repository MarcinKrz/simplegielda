package pl.krzemarcin.gielda.inwestor;

import java.io.Serializable;
import java.util.HashSet;
import lombok.AllArgsConstructor;
import pl.krzemarcin.GieldaApp;
import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.Rynek;
import pl.krzemarcin.gielda.akcje.Akcja;
import pl.krzemarcin.gielda.akcje.GieldaAkcji;
import pl.krzemarcin.gielda.wyjatek.NiewystarczajacyBudzet;
import java.util.Random;
import java.util.Set;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.fundusze.JednostkaUczestnictwa;
import pl.krzemarcin.gielda.surowce.Surowiec;
import pl.krzemarcin.gielda.waluty.Waluta;

/**
 * klasa implementuj�ca logike dzia�ania w�tku typu Inwestor
 *
 * @author Marcin
 */
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InwestorThreadLogic extends Thread implements Serializable {

    private static final Random RANDOM = new Random();

    private Inwestor inwestor;
    private int sleep;

    @Override
    public void run() {
        int losowosc = RANDOM.nextInt(120);
        int kupCzySprzedaj = RANDOM.nextInt(100);
        Set<Aktywa> posiadane = inwestor.getPosiadaneAktywa();
        if ((inwestor.getBudzet() < 1000 || kupCzySprzedaj < 30) && (posiadane.size() > 0)) {
            sprzedaj(posiadane);
        } else {
            if (losowosc < 30) {
                kupAkcje();
            } else if (losowosc < 60) {
                kupWalute();
            } else if (losowosc < 90) {
                kupSurowiec();
            } else {
                kupJednostkeUczestnictwa();

            }
        }
        GieldaApp.sleep(sleep);

        if (RANDOM.nextInt(30) % 5 == 0) {
            zwiekszBudzet();
        }

    }

    /**
     * metoda sprzedania aktywa przez Inwestora
     *
     * @param posiadane zbi�r posiadanych Aktyw Inwestora
     */
    private void sprzedaj(Set<Aktywa> posiadane) {
        Aktywa doSprzedania = new HashSet<>(posiadane).stream()
                .filter(aktywo -> !aktywo.isJestNaRynku())
                .findAny()
                .orElse(null);
        if (doSprzedania != null) {
            DzialaczRynkowy tworca = doSprzedania.getTworca();
            Rynek rynek = tworca.getRynek();
            rynek.wystawOferte(doSprzedania);
        }
    }

    /**
     * metoda zwiekszaj�ca budzet Inwestora
     */
    private void zwiekszBudzet() {
        int dodatkowyBudzet = RANDOM.nextInt(1000) + 1000;
        inwestor.zwiekszBudzet(dodatkowyBudzet);
    }

    /**
     * metoda kupna akcji przez inwestora
     */
    private void kupAkcje() {
        if (GieldaApp.GIELDY_AKCJI.size() > 0) {
            final int gieldaIdx = RANDOM.nextInt(GieldaApp.GIELDY_AKCJI.size());
            final GieldaAkcji gieldaAkcji = GieldaApp.GIELDY_AKCJI.get(gieldaIdx);

            final Set<Akcja> aktywa = gieldaAkcji.aktywaNaRynku();
            final Akcja akcjaDlaInwestora = aktywa.stream()
                    .filter(aktywo -> aktywo.getWlasciciel() != inwestor)
                    .filter(aktywo -> aktywo.getCenaCalkowita() <= inwestor.getBudzet())
                    .findAny()
                    .orElse(null);
            if (akcjaDlaInwestora != null) {
                try {
                    inwestor.kup(gieldaAkcji, akcjaDlaInwestora);
                } catch (NiewystarczajacyBudzet niewystarczajacyBudzet) {

                }
            }

        }
    }

    /**
     * metoda kupna Waluty przez inwestora
     */
    private void kupWalute() {
        Set<DzialaczRynkowy<Waluta>> rodzajeWalut = GieldaApp.RYNEK_WALUT.getAktywaRynku().keySet();
        if (rodzajeWalut.size() > 0) {
            DzialaczRynkowy<Waluta> walutaDoKupna = rodzajeWalut.stream()
                    .findAny()
                    .orElse(null);
            Integer ilosc = RANDOM.nextInt(20) + 1;
            try {
                GieldaApp.RYNEK_WALUT.kup(inwestor, walutaDoKupna, ilosc);
            } catch (NiewystarczajacyBudzet ex) {

            }

        }
    }

    /**
     * metoda kupna jednostki uczestnictwa przez inwestora
     */
    private void kupJednostkeUczestnictwa() {
        Set<DzialaczRynkowy<JednostkaUczestnictwa>> rodzajeFunduszy = GieldaApp.RYNEK_FUNDUSZY.getAktywaRynku().keySet();
        if (rodzajeFunduszy.size() > 0) {
            DzialaczRynkowy<JednostkaUczestnictwa> funduszDoKupna = rodzajeFunduszy.stream()
                    .findAny()
                    .orElse(null);
            Integer ilosc = RANDOM.nextInt(20) + 1;
            try {
                GieldaApp.RYNEK_FUNDUSZY.kup(inwestor, funduszDoKupna, ilosc);
            } catch (NiewystarczajacyBudzet ex) {

            }
        }
    }

    /**
     * metoda kupna surowca przez inwestora
     */
    private void kupSurowiec() {
        Set<DzialaczRynkowy<Surowiec>> rodzajeSurowcow = GieldaApp.RYNEK_SUROWCOW.getAktywaRynku().keySet();
        if (rodzajeSurowcow.size() > 0) {
            DzialaczRynkowy<Surowiec> surowiecDoKupna = rodzajeSurowcow.stream()
                    .findAny()
                    .orElse(null);
            Integer ilosc = RANDOM.nextInt(20) + 1;
            try {
                GieldaApp.RYNEK_SUROWCOW.kup(inwestor, surowiecDoKupna, ilosc);
            } catch (NiewystarczajacyBudzet ex) {

            }

        }
    }

}
