package pl.krzemarcin.gielda.inwestor;

import java.io.Serializable;
import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.Wlasciciel;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.krzemarcin.GieldaApp;
import pl.krzemarcin.InfinitLoopThread;

/**
 * klasa implementuj�ca w�a�ciciela aktyw , mog�cego kupowa� na wszystkich
 * rynkach
 *
 * @author Marcin
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Inwestor extends Wlasciciel<Aktywa> implements Serializable {

    private String imie;
    private String nazwisko;
    private String pesel;

    public Inwestor(String imie, String nazwisko, String pesel, float budzet) {
        super(budzet);
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;

    }

    /**
     * zwraca nazwisko inwestora
     *
     * @return
     */
    @Override
    public String getNazwa() {
        return nazwisko;
    }

    private <R extends Aktywa>
            Set<R> pobierzAktywa(Class<R> clazz) {
        return (Set<R>) posiadaneAktywa.stream()
                .filter(clazz::isInstance)
                .map(a -> (R) a)
                .collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return (imie + " " + nazwisko);
    }

    /**
     * metoda zwiekszaj�ca budzet Inwestora
     *
     * @param dodatkowyBudzet kwota o jak� ma zosta� zwi�kszony bud�et
     */
    public synchronized void zwiekszBudzet(int dodatkowyBudzet) {
        Float nowyBudzet = getBudzet() + dodatkowyBudzet;
        setBudzet(nowyBudzet);
    }

    /**
     * usuniecie inwestora
     */
    public synchronized void usun() {
        InfinitLoopThread inwestorDoUsuniecia = GieldaApp.INWESTORZY.get(this);
        inwestorDoUsuniecia.stopRunning();
        GieldaApp.INWESTORZY.remove(this);

    }
}
