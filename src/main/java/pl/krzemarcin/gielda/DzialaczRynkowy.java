/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin.gielda;

import java.util.Date;
import java.util.Map;

/**
 *
 * Interfejs opisuj�cy typy aktyw , kt�re mo�na kupi� oraz sprzeda� na rynku
 *
 * @param <T>
 */
public interface DzialaczRynkowy<T extends Aktywa> extends Kurs {

    public String getNazwa();

    public Map<Date, Integer> getHistoriaSprzedazy();

    public Map<Date, Integer> getHistoriaOfertSprzedazy();

    public void zarejestrujSprzedaz(Integer ilosc);

    public void zarejestrujOferteSprzedazy(Integer ilosc);

    public T wytworzAktywa(Integer ilosc);

    public Rynek<T> getRynek();
}
