package pl.krzemarcin.gielda.surowce;

import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.Wlasciciel;

/**
 * klasa reprezentuj�ca pewn� ilo�� surowca wystawionego do obiegu
 *
 * @author Marcin
 */
public class Surowiec extends Aktywa<Surowiec> {

    public Surowiec(CenaSurowca tworca, Wlasciciel wlasciciel, Integer ilosc, String nazwa) {
        super(tworca, wlasciciel, ilosc, nazwa, false);
    }

    /**
     * metoda zwracaj�ca ��czn� cene surowca
     *
     * @return
     */
    @Override
    public float getCenaJd() {
        return this.getTworca().getKurs();
    }
}
