package pl.krzemarcin.gielda.surowce;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.Rynek;

/**
 * implementacja interfejsu DziałaczRynkowy służąca do zachowywania historii
 * ceny oraz historii ofert sprzedaży,klasa odpowiedzialna jest za dostarczanie
 * nowych Surowców
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CenaSurowca implements DzialaczRynkowy<Surowiec>, Serializable {

    private String nazwa;
    private String jednostka;
    private Float kurs;
    private Float minKurs;
    private Float maxKurs;
    private Rynek<Surowiec> rynek;
    private Map<Date, Integer> historiaSprzedazy = new HashMap<>();
    private Map<Date, Float> historiaKursu = new HashMap<>();
    private Map<Date, Integer> historiaOfertSprzedazy = new HashMap<>();

    public CenaSurowca(String nazwa, String jednostka, Float kurs, Rynek<Surowiec> rynek) {
        this.nazwa = nazwa;
        this.jednostka = jednostka;
        this.kurs = kurs;
        this.minKurs = kurs;
        this.maxKurs = kurs;
        historiaKursu.put(new Date(), this.kurs);
        this.rynek = rynek;
    }

    /**
     * moetoda ustawiająca kurs Surowca
     *
     * @param nowaCena
     */
    @Override
    public void setKurs(Float nowaCena) {
        this.kurs = nowaCena;
        historiaKursu.put(new Date(), this.kurs);
        if (this.kurs < minKurs) {
            this.minKurs = kurs;
        }
        if (this.kurs > maxKurs) {
            this.maxKurs = kurs;
        }
    }

    /**
     * metoda rejestrująca w historii operacje kupna surwoca
     *
     * @param ilosc
     */
    @Override
    public void zarejestrujSprzedaz(Integer ilosc) {
        historiaSprzedazy.put(new Date(), ilosc);
    }

    /**
     * metoda odpowiadająca za operacje wytworzenia surowca
     *
     * @param ilosc
     * @return
     */
    @Override
    public Surowiec wytworzAktywa(Integer ilosc) {
        Surowiec surowiec = new Surowiec(this, null, ilosc, this.nazwa);
        zarejestrujOferteSprzedazy(ilosc);
        return surowiec;
    }

    /**
     * metoda rejestrująca w historii wystawienie na sprzedaż surowca
     *
     * @param ilosc
     */
    @Override
    public void zarejestrujOferteSprzedazy(Integer ilosc) {
        historiaOfertSprzedazy.put(new Date(), ilosc);
    }

    /**
     * zwraca rynek na którym jest notowany surowiec
     *
     * @return
     */
    @Override
    public Rynek<Surowiec> getRynek() {
        return rynek;
    }

    @Override
    public String toString() {
        return getNazwa();
    }
}
