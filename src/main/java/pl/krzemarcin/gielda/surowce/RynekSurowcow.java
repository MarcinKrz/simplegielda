package pl.krzemarcin.gielda.surowce;

import pl.krzemarcin.gielda.Rynek;

/**
 * klasa implementująca klase Rynek , reprezentująca Rynek Surowców
 *
 * @author Marcin
 */
public class RynekSurowcow extends Rynek<Surowiec> {

    public RynekSurowcow(String nazwa, Float marza) {
        super(nazwa, marza);
    }

}
