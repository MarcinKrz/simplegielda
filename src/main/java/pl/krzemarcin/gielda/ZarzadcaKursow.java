/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin.gielda;

import java.util.Calendar;
import java.util.stream.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import pl.krzemarcin.gielda.DzialaczRynkowy;

/**
 *
 * klasa odpowiadająca za wyliczenie kursu Aktywa
 */
public class ZarzadcaKursow {

    /**
     * metoda wyliczjąca kurs dla działacza podanego jako parametr
     *
     * @param dzialacz działaczRynkowy dla którego ma zsotać wyliczony nowy kurs
     */
    public void wyliczKurs(DzialaczRynkowy dzialacz) {
        Random RANDOM = new Random();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.SECOND, -30);
        Date nowMinus30Sek = c.getTime();

        Map<Date, Integer> daty = dzialacz.getHistoriaOfertSprzedazy();
        List<Integer> ofertyZOstatnich30Sek = new HashSet<>(daty.keySet())
                .stream()
                .filter(date -> date.after(nowMinus30Sek))
                .map(date -> daty.get(date))
                .collect(Collectors.toList());

        Map<Date, Integer> datySprzedazy = dzialacz.getHistoriaSprzedazy();
        List<Integer> sprzedazeZOstatnich30Sek = datySprzedazy.keySet()
                .stream()
                .filter(date -> date.after(nowMinus30Sek))
                .map(date -> datySprzedazy.get(date))
                .collect(Collectors.toList());

        int iloscAkcjiWystawionych = 0;
        for (Integer ilosc : ofertyZOstatnich30Sek) {
            iloscAkcjiWystawionych += ilosc;
        }
        int iloscAkcjiKupionych = 0;
        for (Integer ilosc : sprzedazeZOstatnich30Sek) {
            iloscAkcjiKupionych += ilosc;
        }
        if (iloscAkcjiKupionych * 2 > iloscAkcjiWystawionych) {
            dzialacz.setKurs(dzialacz.getKurs() * 1.02f);
        } else {
            dzialacz.setKurs(dzialacz.getKurs() * 0.98f);
        }
        if (RANDOM.nextInt(100) % 5 == 0) {
            dzialacz.setKurs(dzialacz.getKurs() * 1.09f);
        } else if (RANDOM.nextInt(100) % 5 == 0) {
            dzialacz.setKurs(dzialacz.getKurs() * 0.91f);

        }
    }

}
