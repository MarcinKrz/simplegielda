package pl.krzemarcin.gielda;

import java.io.Serializable;
import lombok.Getter;
import pl.krzemarcin.gielda.wyjatek.NiewystarczajacyBudzet;

import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * klasa abstrakcyjna zawieraj�ca metody obiektu kt�ry mo�e posiada� aktywa i
 * wystawia� je na rynkach
 *
 * @param <T> typ Aktyw posiadanych przez objekt
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class Wlasciciel<T extends Aktywa> implements Serializable {

    @Getter
    protected final Set<T> posiadaneAktywa = new HashSet<>();
    @Getter
    @Setter
    private volatile Float budzet = 0f;

    /**
     *
     * @param budzet budzet posiadany przez W�a�ciciela
     */
    public Wlasciciel(float budzet) {
        this.budzet = budzet;
    }

    public abstract String getNazwa();

    /**
     *
     * @param <R> rodzaj Aktywa do kupna
     * @param rynek Rynek na kt�rym chcemy kupi� aktywo
     * @param aktywo Aktywo do kupna
     * @throws NiewystarczajacyBudzet
     */
    public <R extends Aktywa<R>> void kup(Rynek<R> rynek, R aktywo) throws NiewystarczajacyBudzet {
        rynek.kup(this, aktywo.getTworca(), aktywo.getIlosc());
    }

    /**
     *
     * @param wartosc wartosc pieni�dzy do pobrania
     * @throws NiewystarczajacyBudzet wyj�tek rzucany gdy kupujacy posiada za
     * ma�o pieniedzy
     */
    public synchronized void pobierzPieniadze(float wartosc) throws NiewystarczajacyBudzet {
        if (budzet < wartosc) {
            throw new NiewystarczajacyBudzet();
        } else {
            budzet -= wartosc;
        }
    }

    /**
     * dokonanie sprzeda�y aktywa
     *
     * @param aktywo aktywo do sprzeda�y
     * @param cenaAktywa cena jak� dostaje W�a�ciciel za sprzeda� Aktywa
     */
    public synchronized void dokonajSprzedazy(T aktywo, float cenaAktywa) {
        final boolean removed = posiadaneAktywa.remove(aktywo);
        if (removed) {
            aktywo.setWlasciciel(null);
            budzet += cenaAktywa;
        }
    }

    /**
     * dodanie aktywa do listy posiadanych Aktyw
     *
     * @param aktywo objekt akytwa do dodania
     */
    public void dodajAktywo(T aktywo) {
        aktywo.setWlasciciel(this);
        posiadaneAktywa.add(aktywo);
    }

    /**
     * wystawienie aktywa na rynek na sprzeda�
     *
     * @param rynek Rynek , na kt�ry zostanie wystawione aktywo
     * @param aktywo aktywo do wystaawienia na sprzedaz
     */
    public void wypuscAktywa(Rynek<T> rynek, T aktywo) {
        rynek.wystawOferte(aktywo);

    }

}
