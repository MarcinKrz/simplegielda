package pl.krzemarcin.gielda;

import java.io.Serializable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * klasa abstrakcyjna reprezentuj�ca aktywo,czyli typ obiektu jaki mo�e zosta�
 * wystawiony na rynku
 *
 * @author Marcin
 * @param <T>
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class Aktywa<T> implements Serializable {

    private DzialaczRynkowy tworca;
    @Setter
    private volatile Wlasciciel wlasciciel;
    private int ilosc;
    private String nazwa;
    @Setter
    private boolean jestNaRynku;

    public abstract float getCenaJd();

    /**
     * metoda zwraca cene aktywa
     *
     * @return
     */
    public float getCenaCalkowita() {
        return getCenaJd() * ilosc;
    }

    @Override
    public String toString() {
        return ("nazwa:" + nazwa + " ilosc:" + ilosc);
    }
}
