package pl.krzemarcin.gielda.akcje;

import java.io.Serializable;
import lombok.AllArgsConstructor;

import java.util.Random;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * klasa implementuj�ca logike dzia�ania w�tku typu Spolka, odpowiada za
 * symulacje wypuszczania Akcji na Gie�de
 *
 * @author Marcin
 */
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SpolkaThreadLogic implements Runnable, Serializable {

    private static final Random RANDOM = new Random();

    private Spolka spolka;
    private Integer sleep;

    @Override
    public void run() {

        int iloscAkcji = RANDOM.nextInt(20);
        while (iloscAkcji == 0) {
            iloscAkcji = RANDOM.nextInt(20);
        }

        spolka.wypuscAktywa(spolka.getRynek(), spolka.wytworzAktywa(iloscAkcji));
        spolka.wygenerujPrzychodIZysk();
        sleep(sleep);

    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }
}
