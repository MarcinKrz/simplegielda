package pl.krzemarcin.gielda.akcje;

import lombok.Getter;
import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.Wlasciciel;

/**
 * implementacja klasy @Aktywa reprezentuj�ca akcj� sp�ki do wystawiania na
 * rynku Akcji
 *
 * @author Marcin
 */
@Getter
public class Akcja extends Aktywa<Akcja> {

    private Spolka spolka;

    /**
     *
     * @param wlasciciel wlasciciel Akcji
     * @param ilosc ilosc Akcji
     * @param nazwa obiektu
     * @param spolka spolka do kt�rej b�dzie nale�a� obiekt
     */
    public Akcja(Wlasciciel wlasciciel, int ilosc, String nazwa, Spolka spolka) {
        super(spolka, wlasciciel, ilosc, nazwa, false);
        this.spolka = spolka;
    }

    /**
     * metoda zwraca cene ca�kowit� Akcji
     *
     * @return
     */
    @Override
    public float getCenaJd() {
        return spolka.getKurs();
    }
}
