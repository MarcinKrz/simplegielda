/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin.gielda.akcje;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

/**
 * klasa implementuj�ca indeks gie�dowy czyli zbi�r wybranych sp�ek na gie�dzie
 * i okre�laj�ca ich ��czn� warto��
 *
 * @author Marcin
 */
@Getter
@Setter
public class Indeks implements Serializable {

    private String nazwa;
    private Set<Spolka> listaSpolek = new HashSet<>();

    public Indeks(String nazwa, Set<Spolka> listaSpolek) {
        this.nazwa = nazwa;
        this.listaSpolek = listaSpolek;

    }

    /**
     * metoda zwraca wartosc Indeksu
     *
     * @return
     */
    public Double getWartoscIndeksu() {
        return listaSpolek.stream()
                .mapToDouble(spolka -> spolka.getKurs() * spolka.getIloscAkcji())
                .sum();
    }

    @Override
    public String toString() {
        return nazwa;
    }

}
