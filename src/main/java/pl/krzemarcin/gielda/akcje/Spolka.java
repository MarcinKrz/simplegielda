package pl.krzemarcin.gielda.akcje;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.krzemarcin.GieldaApp;
import pl.krzemarcin.InfinitLoopThread;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.Rynek;
import pl.krzemarcin.gielda.Wlasciciel;
import pl.krzemarcin.gielda.wyjatek.NiewystarczajacyBudzet;

/**
 * klasa implementuj�ca w�a�ciciela akcji oraz dzia�acza Rynkowego , odowiada za
 * zarz�dzanie wyprodukowanymi przez siebie Akcjami oraz pami�� zmian kurs�w
 * Akcji w czasie
 *
 * @author Marcin
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Spolka extends Wlasciciel<Akcja> implements DzialaczRynkowy<Akcja>, Serializable {

    private String nazwa;
    private Float kurs;
    private Integer iloscAkcji;
    private Float minKurs;
    private Float maxKurs;
    private Date dataPierwszejWyceny;
    private Integer wolumen;
    private Float obroty;
    private Float zysk;
    private Float przychod;
    private Float kapitalWlasny;
    private Float kapitalZakladowy;
    private Rynek<Akcja> rynek;
    private Set<Akcja> wypuszczoneAkcje = new HashSet<>();
    private Map<Date, Float> historiaKursu = new HashMap<>();
    private Map<Date, Integer> historiaSprzedazy = new HashMap<>();
    private Map<Date, Integer> historiaOfertSprzedazy = new HashMap<>();

    public Spolka(float budzet, String nazwa, float cenaAkcji, Rynek<Akcja> rynek) {
        super(budzet);
        this.nazwa = nazwa;
        this.kurs = cenaAkcji;
        this.iloscAkcji = 0;
        this.minKurs = kurs;
        this.maxKurs = kurs;
        this.dataPierwszejWyceny = new Date();
        this.wolumen = 0;
        this.obroty = 0f;
        this.zysk = 0f;
        this.przychod = 0f;
        this.rynek = rynek;
        this.historiaKursu.put(new Date(), this.kurs);

    }

    /**
     * metoda odpowiadaj�ca za tworzenie nowych Akcji spo�ki oraz rejestracji
     * ich w historii sp�ki
     *
     * @param ilosc ilo�� Akcji do wytworzenia
     * @return stworzona Akcja
     */
    @Override
    public synchronized Akcja wytworzAktywa(Integer ilosc) {
        Akcja akcja = new Akcja(this, ilosc, nazwa, this);
        iloscAkcji += ilosc;
        dodajAktywo(akcja);
        this.posiadaneAktywa.add(akcja);
        this.wypuszczoneAkcje.add(akcja);
        return akcja;
    }

    /**
     * ustawia warto�� kursu Akcji
     *
     * @param nowaCena nowa cena Akcji
     */
    @Override
    public void setKurs(Float nowaCena) {
        this.kurs = nowaCena;
        historiaKursu.put(new Date(), this.kurs);
        if (this.kurs < minKurs) {
            this.minKurs = kurs;
        }
        if (this.kurs > maxKurs) {
            this.maxKurs = kurs;
        }
    }

    /**
     * metoda odpowiadaj�ca za rejestrowanie kupna akcji sp�ki
     *
     * @param ilosc ilo�� sprzedanych akcji
     */
    @Override
    public void zarejestrujSprzedaz(Integer ilosc) {
        historiaSprzedazy.put(new Date(), ilosc);
        wolumen += ilosc;
        obroty += ilosc * this.kurs;
    }

    /**
     * zarejestrowanie oferty sprzeda�y akcji sp�ki
     *
     * @param ilosc ilo�� wystawionych akcji na sprzeda�
     */
    @Override
    public void zarejestrujOferteSprzedazy(Integer ilosc) {
        historiaOfertSprzedazy.put(new Date(), ilosc);
    }

    /**
     * wykupienie akcji przez sp�k� po podanej cenie wykupu
     *
     * @param cenaWykupu cena wykupu jednej akcji
     * @throws NiewystarczajacyBudzet rzucany gdy sp�ki nie sta� na wykupienie
     * po podanej cenie wykupu
     *
     */
    public synchronized void wykupAkcje(Float cenaWykupu) throws NiewystarczajacyBudzet {
        int lacznaCena = 0;
        for (Akcja akcja : wypuszczoneAkcje) {
            if (!this.equals(akcja.getWlasciciel())) {
                lacznaCena += cenaWykupu * akcja.getIlosc();
            }
        }
        if (lacznaCena <= getBudzet()) {
            for (Akcja akcja : wypuszczoneAkcje) {
                if (akcja.isJestNaRynku()) {
                    akcja.getTworca().getRynek().wycofaj(akcja);
                    akcja.setJestNaRynku(false);
                }
                Wlasciciel wlasciciel = akcja.getWlasciciel();
                if (!this.equals(wlasciciel)) {
                    Float cena = akcja.getIlosc() * cenaWykupu;
                    pobierzPieniadze(cena);
                    wlasciciel.dokonajSprzedazy(akcja, cena);
                }
            }
            posiadaneAktywa.removeAll(posiadaneAktywa);
            wypuszczoneAkcje.removeAll(wypuszczoneAkcje);
        } else {
            throw new NiewystarczajacyBudzet();
        }
    }

    /**
     * metoda usuniecia sp�ki
     */
    public synchronized void usun() { // poprawi� usuwanie z rynku
        InfinitLoopThread watekSpolki = GieldaApp.SPOLKI.get(this);
        watekSpolki.stopRunning();
        GieldaApp.SPOLKI.remove(this);
        int ilosc = 0;
        for (Akcja akcja : wypuszczoneAkcje) {
            if (!this.equals(akcja.getWlasciciel())) {
                ilosc += akcja.getIlosc();
            }
        }
        float cenaWykupu = 0;
        if (ilosc * getKurs() > getBudzet()) {
            cenaWykupu = (0.5f * getBudzet()) / (ilosc + 1);
        } else {
            cenaWykupu = getKurs();
        }
        try {
            wykupAkcje(cenaWykupu);
        } catch (NiewystarczajacyBudzet ex) {
        }
        getRynek().getAktywaRynku().remove(this);
        GieldaAkcji gielda = (GieldaAkcji) getRynek();
        Set<Indeks> zbiorIndeksow = gielda.getListaIndeksow();
        List<Indeks> indeksyZSpolka = zbiorIndeksow.stream()
                .filter(indeks -> indeks.getListaSpolek().contains(this))
                .collect(Collectors.toList());
        for (Indeks indeks : indeksyZSpolka) {
            indeks.getListaSpolek().remove(this);
        }

    }

    /**
     * metoda odpowiadaj�ca ze wygenerowanie przchodu oraz zysku sp�ki
     */
    public void wygenerujPrzychodIZysk() {
        Random RANDOM = new Random();
        zysk = (RANDOM.nextFloat() + 1) * (RANDOM.nextInt(10000) + 1000);
        przychod = zysk + (RANDOM.nextFloat() * (RANDOM.nextInt(10000) + 1));
        kapitalWlasny = getBudzet() * 0.55f;
        kapitalZakladowy = getBudzet() * 0.45f;

    }

    /**
     * zwraca rynek na kt�rym jest notowana sp�ka
     *
     * @return
     */
    @Override
    public Rynek<Akcja> getRynek() {
        return rynek;
    }

    public String toString() {
        return nazwa;
    }

}
