package pl.krzemarcin.gielda.akcje;

import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.Rynek;
import pl.krzemarcin.gielda.Wlasciciel;
import pl.krzemarcin.gielda.wyjatek.NiewystarczajacyBudzet;

/**
 * klasa implementuj�ca klase @Rynek , reprezentuj�ca Gie�de Papier�w
 * Warto�ciowych
 *
 * @author Marcin
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GieldaAkcji extends Rynek<Akcja> {

    private String kraj;
    private String miasto;
    private Set<Indeks> listaIndeksow = new HashSet<>();

    public GieldaAkcji(String nazwa, String kraj, String miasto, Float marza) {
        super(nazwa, marza);
        this.kraj = kraj;
        this.miasto = miasto;
    }

    /**
     * metoda implementuj�ca operacje kupna na rynku
     *
     * @param kupujacy obiekt, kt�ry chce kupi� aktywo
     * @param rodzaj rodzaj Akywa
     * @param ilosc ilosc Aktywa do kupna
     * @throws NiewystarczajacyBudzet
     */
    @Override
    public synchronized void kup(Wlasciciel kupujacy, DzialaczRynkowy rodzaj, Integer ilosc) throws NiewystarczajacyBudzet {
        Set<Akcja> aktywaRodzaju = getAktywaRynku().get(rodzaj);
        Akcja aktywo = aktywaRodzaju.stream()
                .filter(aktyw -> ilosc == aktyw.getIlosc())
                .findAny()
                .orElse(null);
        if (aktywo != null) {
            final Akcja wycofaneAktywo = wycofaj(aktywo);

            DzialaczRynkowy tworca = aktywo.getTworca();

            final Wlasciciel sprzedajacy = aktywo.getWlasciciel();
            if (wycofaneAktywo != null) {
                final float cenaAktywa = aktywo.getCenaCalkowita();
                final float cenaAktywaMinusMarza = cenaAktywa * (1 - (getMarza() / 100));
                try {
                    kupujacy.pobierzPieniadze(cenaAktywa);
                    sprzedajacy.dokonajSprzedazy(aktywo, cenaAktywaMinusMarza);
                    kupujacy.dodajAktywo(aktywo);
                    aktywo.setJestNaRynku(false);
                    tworca.zarejestrujSprzedaz(aktywo.getIlosc());
                } catch (NiewystarczajacyBudzet ex) {
                    wstawPonownie(aktywo);

                }
            }
        }
    }

    /**
     * metoda dodaj�ca indeks do gie�dy
     *
     * @param indeks indeks ,kt�ry ma zosta� dodany
     */
    public void dodajIndeks(Indeks indeks) {
        listaIndeksow.add(indeks);
    }

    /**
     * metoda usuwaj�ca indeks z gie�dy
     *
     * @param indeks indeks kt�ry ma zosta� usuni�ty
     */
    public void usunIndeks(Indeks indeks) {
        listaIndeksow.remove(indeks);
    }

}
