/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin.gielda;

import java.util.Date;
import java.util.Map;

/**
 * interfejs reprezentujący metody potrzebne do zarządzania kursami aktyw
 *
 * @author Marcin
 */
public interface Kurs {

    public Float getKurs();

    public void setKurs(Float Kurs);

    public Map<Date, Float> getHistoriaKursu();

}
