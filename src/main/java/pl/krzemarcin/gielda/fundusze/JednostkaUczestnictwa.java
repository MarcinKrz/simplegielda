/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin.gielda.fundusze;

import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.Wlasciciel;

/**
 * klasa implementująca Aktywa reprezentująca Jednoste uczestnictwa
 *
 * @author Marcin
 */
public class JednostkaUczestnictwa extends Aktywa<JednostkaUczestnictwa> {

    public JednostkaUczestnictwa(DzialaczRynkowy tworca, Wlasciciel wlasciciel, int ilosc, String nazwa, boolean jestNaRynku) {
        super(tworca, wlasciciel, ilosc, nazwa, jestNaRynku);
    }

    /**
     * metoda zwraca łączną wartość obiektu
     *
     * @return
     */
    @Override
    public float getCenaJd() {
        return getTworca().getKurs();
    }

}
