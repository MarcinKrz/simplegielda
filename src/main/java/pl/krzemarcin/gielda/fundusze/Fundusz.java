/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin.gielda.fundusze;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.krzemarcin.GieldaApp;
import pl.krzemarcin.InfinitLoopThread;
import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.Rynek;
import pl.krzemarcin.gielda.Wlasciciel;

/**
 * klasa implementuj��a w�a�ciciela Aktyw oraz dzia�acza rynkowego produkuj�cego
 * Jednsotki Uczestnictwa, odowiada za zarz�dzanie wyprodukowanymi przez siebie
 * Jednsotkami Uczestnictwa oraz pami�� zmian ich kurs�w w czasie
 *
 * @author Marcin
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Fundusz extends Wlasciciel<Aktywa> implements DzialaczRynkowy<JednostkaUczestnictwa>, Serializable {

    private String imie;
    private String nazwisko;
    private String nazwa;
    private Float kurs;
    private Float minKurs;
    private Float maxKurs;
    private int iloscJednostekUczestnictwa;
    private Rynek<JednostkaUczestnictwa> rynek;
    private Map<Date, Float> historiaKursu = new HashMap<>();
    private Map<Date, Integer> historiaSprzedazy = new HashMap<>();
    private Map<Date, Integer> historiaOfertSprzedazy = new HashMap<>();
    private Set<JednostkaUczestnictwa> wypuszczoneJednostkiUczestnictwa = new HashSet<>();

    public Fundusz(String nazwa, String imie, String nazwisko, float budzet, Float kurs, Rynek<JednostkaUczestnictwa> rynek) {

        super(budzet);
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nazwa = nazwa;
        this.kurs = kurs;
        this.minKurs = kurs;
        this.maxKurs = kurs;
        this.rynek = rynek;
        this.iloscJednostekUczestnictwa = 0;
        this.historiaKursu.put(new Date(), this.kurs);
    }

    /**
     * zwraca nazwe sp�ki
     *
     * @return
     */
    @Override
    public String getNazwa() {
        return nazwa;
    }

    /**
     * metoda odpowiadaj�ca za tworzenie nowych Jednostek Uczestnictwa Funduszu
     * oraz rejestracji ich w historii Funduszu
     *
     * @param ilosc Jednostek do wytworzenia
     * @return
     */
    @Override
    public synchronized JednostkaUczestnictwa wytworzAktywa(Integer ilosc) {
        JednostkaUczestnictwa jednostkaUczestnictwa = new JednostkaUczestnictwa(this, this, ilosc, nazwa, false);
        iloscJednostekUczestnictwa += ilosc;
        this.wypuszczoneJednostkiUczestnictwa.add(jednostkaUczestnictwa);
        zarejestrujOferteSprzedazy(ilosc);
        return jednostkaUczestnictwa;
    }

    /**
     * ustawia warto�� kursu Jednostki Uczestnictwa
     *
     * @param nowaCena
     */
    @Override
    public void setKurs(Float nowaCena) {
        this.kurs = nowaCena;
        historiaKursu.put(new Date(), this.kurs);
        if (this.kurs < minKurs) {
            this.minKurs = kurs;
        }
        if (this.kurs > maxKurs) {
            this.maxKurs = kurs;
        }
    }

    /**
     * metoda odpowiadaj�ca za rejestrowanie kupna Jednostki Uczestnictwa sp�ki
     *
     * @param ilosc ilosc kupionych Jednostek Uczestnictwa
     */
    @Override
    public void zarejestrujSprzedaz(Integer ilosc) {
        historiaSprzedazy.put(new Date(), ilosc);
    }

    /**
     * zarejestrowanie oferty sprzeda�y Jednostki Uczestnictwa Funduszu
     *
     * @param ilosc ilosc Jednsotek Uczestnictwa wystawionych na sprzeda�
     */
    @Override
    public void zarejestrujOferteSprzedazy(Integer ilosc) {
        historiaOfertSprzedazy.put(new Date(), ilosc);
    }

    /**
     * metoda odpowiadajaca za wypuszczenie na rynek obiektu typu
     * JednostkaUczestnictwa
     *
     * @param rynek
     * @param ju
     */
    public void wypuscJednostkaUczestnictwa(Rynek<JednostkaUczestnictwa> rynek, JednostkaUczestnictwa ju) { //co z t� metod� 
        rynek.wystawOferte(ju);
    }

    /**
     * metoda usuwaj�ca obiekt
     */
    public void usun() {
        InfinitLoopThread watekFunduszu = GieldaApp.FUNDUSZE.get(this);
        watekFunduszu.stopRunning();
        wykup(0f);
        GieldaApp.FUNDUSZE.remove(this);
        GieldaApp.RYNEK_FUNDUSZY.getAktywaRynku().remove(this);
    }

    /**
     * metoda wykupienia wytworzonych przez Fundusz Jednostek Uczestnictwa
     *
     * @param cenaWykupu cena wykupu jednostki uczestnictwa
     */
    public synchronized void wykup(Float cenaWykupu) {
        for (JednostkaUczestnictwa jednostka : wypuszczoneJednostkiUczestnictwa) {
            if (jednostka.isJestNaRynku()) {
                jednostka.getTworca().getRynek().wycofaj(jednostka);
                jednostka.setJestNaRynku(false);
            }
            Wlasciciel wlasciciel = jednostka.getWlasciciel();
            if (!this.equals(wlasciciel)) {
                wlasciciel.dokonajSprzedazy(jednostka, 0);
            }
        }
        posiadaneAktywa.removeAll(posiadaneAktywa);
        wypuszczoneJednostkiUczestnictwa.removeAll(wypuszczoneJednostkiUczestnictwa);
    }

    @Override
    public String toString() {
        return nazwa;
    }

}
