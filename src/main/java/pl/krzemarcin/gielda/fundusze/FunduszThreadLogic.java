/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.krzemarcin.gielda.fundusze;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import pl.krzemarcin.GieldaApp;
import pl.krzemarcin.gielda.Aktywa;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.Rynek;
import pl.krzemarcin.gielda.akcje.Akcja;
import pl.krzemarcin.gielda.akcje.GieldaAkcji;
import pl.krzemarcin.gielda.surowce.Surowiec;
import pl.krzemarcin.gielda.waluty.Waluta;
import pl.krzemarcin.gielda.wyjatek.NiewystarczajacyBudzet;

/**
 * klasa implementuj�ca logike dzia�ania w�tku typu Fundusz , odpowiada za
 * operacje kupna i sprzeda�y aktyw na rynkach
 *
 * @author Marcin
 */
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FunduszThreadLogic extends Thread implements Serializable {

    private Fundusz fundusz;
    private Integer sleep;
    private static final Random RANDOM = new Random();

    @Override
    public void run() {

        int losowosc = RANDOM.nextInt(100);
        int kupCzySprzedaj = RANDOM.nextInt(100);
        Set<Aktywa> posiadane = fundusz.getPosiadaneAktywa();
        if ((fundusz.getBudzet() < 1000 || kupCzySprzedaj < 30) && (posiadane.size() > 0)) {
            sprzedaj(posiadane);
        } else {
            if (losowosc < 33) {
                kupAkcje();
            } else if (losowosc < 66) {
                kupWalute();
            } else {
                kupSurowiec();
            }
        }
        this.sleep(sleep);
    }

    /**
     * metoda kupna Surowca przez Fundusz
     */
    private void kupSurowiec() {
        Set<DzialaczRynkowy<Surowiec>> rodzajeSurowcow = GieldaApp.RYNEK_SUROWCOW.getAktywaRynku().keySet();
        if (rodzajeSurowcow.size() > 0) {
            DzialaczRynkowy<Surowiec> surowiecDoKupna = rodzajeSurowcow.stream()
                    .findAny()
                    .orElse(null);
            Integer ilosc = RANDOM.nextInt(20) + 1;
            try {
                GieldaApp.RYNEK_SUROWCOW.kup(fundusz, surowiecDoKupna, ilosc);
            } catch (NiewystarczajacyBudzet ex) {

            }

        }
    }

    /**
     * metoda kupna Waluty przez Fundusz
     */
    private void kupWalute() {
        Set<DzialaczRynkowy<Waluta>> rodzajeWalut = GieldaApp.RYNEK_WALUT.getAktywaRynku().keySet();
        if (rodzajeWalut.size() > 0) {
            DzialaczRynkowy<Waluta> walutaDoKupna = rodzajeWalut.stream()
                    .findAny()
                    .orElse(null);
            Integer ilosc = RANDOM.nextInt(20) + 1;
            try {
                GieldaApp.RYNEK_WALUT.kup(fundusz, walutaDoKupna, ilosc);
            } catch (NiewystarczajacyBudzet ex) {

            }

        }
    }

    /**
     * metoda kupna Akcji przez Fundusz
     */
    private void kupAkcje() {
        if (GieldaApp.GIELDY_AKCJI.size() > 0) {
            final int gieldaIdx = RANDOM.nextInt(GieldaApp.GIELDY_AKCJI.size());
            final GieldaAkcji gieldaAkcji = GieldaApp.GIELDY_AKCJI.get(gieldaIdx);

            final Set<Akcja> aktywa = gieldaAkcji.aktywaNaRynku();
            final Akcja akcjaDlaInwestora = aktywa.stream()
                    .filter(aktywo -> aktywo.getWlasciciel() != fundusz)
                    .filter(aktywo -> aktywo.getCenaCalkowita() <= fundusz.getBudzet())
                    .findAny()
                    .orElse(null);
            if (akcjaDlaInwestora != null) {
                try {
                    fundusz.kup(gieldaAkcji, akcjaDlaInwestora);
                } catch (NiewystarczajacyBudzet niewystarczajacyBudzet) {

                }
            }

        }
    }

    /**
     * metoda implementuj�ca sprzedanie Aktywa przez Rynek
     *
     * @param posiadane
     */
    private void sprzedaj(Set<Aktywa> posiadane) {
        Aktywa doSprzedania = new HashSet<>(posiadane).stream()
                .filter(aktywo -> !aktywo.isJestNaRynku())
                .findAny()
                .orElse(null);
        if (doSprzedania != null) {
            DzialaczRynkowy tworca = doSprzedania.getTworca();
            Rynek rynek = tworca.getRynek();
            rynek.wystawOferte(doSprzedania);
        }
    }

    /**
     * u�pienie w�tku Funduszu na okre�lony czas podany jako parametr
     *
     * @param millis czas na jaki ma zosta� u�piony w�tek
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }

}
