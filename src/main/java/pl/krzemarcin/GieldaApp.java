package pl.krzemarcin;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import pl.krzemarcin.gielda.DzialaczRynkowy;
import pl.krzemarcin.gielda.ZarzadcaKursow;
import pl.krzemarcin.gielda.akcje.Akcja;
import pl.krzemarcin.gielda.akcje.GieldaAkcji;
import pl.krzemarcin.gielda.akcje.Spolka;
import pl.krzemarcin.gielda.akcje.SpolkaThreadLogic;
import pl.krzemarcin.gielda.fundusze.Fundusz;
import pl.krzemarcin.gielda.fundusze.FunduszThreadLogic;
import pl.krzemarcin.gielda.fundusze.JednostkaUczestnictwa;
import pl.krzemarcin.gielda.fundusze.RynekFunduszy;
import pl.krzemarcin.gielda.inwestor.Inwestor;
import pl.krzemarcin.gielda.inwestor.InwestorThreadLogic;
import pl.krzemarcin.gielda.surowce.CenaSurowca;
import pl.krzemarcin.gielda.surowce.RynekSurowcow;
import pl.krzemarcin.gielda.surowce.Surowiec;
import pl.krzemarcin.gielda.waluty.KursWaluty;
import pl.krzemarcin.gielda.waluty.RynekWalut;
import pl.krzemarcin.gielda.waluty.Waluta;

/**
 *
 * @author Marcin
 */
public class GieldaApp {

    public static Long poczatekSymulacji = System.currentTimeMillis();
    public static volatile List<GieldaAkcji> GIELDY_AKCJI = new ArrayList<>();
    public static volatile RynekSurowcow RYNEK_SUROWCOW;
    public static volatile RynekWalut RYNEK_WALUT;
    public static volatile RynekFunduszy RYNEK_FUNDUSZY;
    public static volatile Map<Inwestor, InfinitLoopThread> INWESTORZY = new HashMap<>();
    public static volatile Map<Fundusz, InfinitLoopThread> FUNDUSZE = new HashMap<>();
    public static volatile Map<Spolka, InfinitLoopThread> SPOLKI = new HashMap<>();

    private static final Random random = new Random();

    public static List<String> bazaNazwSpolek = new ArrayList<>();
    public static List<String[]> bazaImionINazwisk = new ArrayList<>();
    public static List<String> bazaNrPesel = new ArrayList<>();
    public static List<String[]> bazaNazwSurowcow = new ArrayList<>();
    public static List<String[]> bazaWalut = new ArrayList<>();
    public static List<String> bazaNazwFunduszy = new ArrayList<>();
    public static List<String[]> bazaMiejscGield = new ArrayList<>();

    public static void main(String[] args) throws InterruptedException, IOException {
        GieldaApp gieldaApp = new GieldaApp();
        OknoAplikacji oknoAplikacji = new OknoAplikacji(gieldaApp);
        gieldaApp.run();
    }

    public GieldaApp() {
    }

    /**
     * metoda zwraca liste sp�ek notowanych na wszystkich gie�dach akcji
     *
     * @return
     */
    public List<Spolka> listaSpolek() {
        return GIELDY_AKCJI.stream()
                .map(gielda -> gielda.getAktywaRynku())
                .map(aktywaRynku -> aktywaRynku.keySet())
                .flatMap(dzialacze -> dzialacze.stream())
                .filter(dzialacz -> dzialacz instanceof Spolka)
                .map(dzialacz -> (Spolka) dzialacz)
                .distinct()
                .collect(Collectors.toList());
    }

    public void run() throws InterruptedException, IOException {

        wczytaj();
        RYNEK_SUROWCOW = new RynekSurowcow("Rynek Surowcow", 10f);
        RYNEK_WALUT = new RynekWalut("Rynek Walut", 10f);
        RYNEK_FUNDUSZY = new RynekFunduszy("Rynek Funduszy", 10f);

        ZarzadcaKursow zarzadcaKursow = new ZarzadcaKursow();
        new Thread(() -> {
            while (true) {
                sleep(6_000);

                for (GieldaAkcji gieldaAkcji : GIELDY_AKCJI) {

                    Set<DzialaczRynkowy<Akcja>> zbiorDzialaczyNaGieldzie = gieldaAkcji.getAktywaRynku().keySet();
                    for (DzialaczRynkowy dzialaczRynkowy : zbiorDzialaczyNaGieldzie) {
                        zarzadcaKursow.wyliczKurs(dzialaczRynkowy);

                    }
                }

                Set<DzialaczRynkowy<Waluta>> zbiorDzialaczyNaRynkuWalut = RYNEK_WALUT.getAktywaRynku().keySet();
                for (DzialaczRynkowy<Waluta> dzialaczRynkowy : zbiorDzialaczyNaRynkuWalut) {
                    zarzadcaKursow.wyliczKurs(dzialaczRynkowy);

                }
                Set<DzialaczRynkowy<Surowiec>> zbiorDzialaczyNaRynkuSurowcow = RYNEK_SUROWCOW.getAktywaRynku().keySet();
                for (DzialaczRynkowy<Surowiec> dzialaczRynkowy : zbiorDzialaczyNaRynkuSurowcow) {
                    zarzadcaKursow.wyliczKurs(dzialaczRynkowy);

                }
                Set<DzialaczRynkowy<JednostkaUczestnictwa>> zbiorDzialaczyNaRynkuFunduszy = RYNEK_FUNDUSZY.getAktywaRynku().keySet();
                for (DzialaczRynkowy<JednostkaUczestnictwa> dzialaczRynkowy : zbiorDzialaczyNaRynkuFunduszy) {
                    zarzadcaKursow.wyliczKurs(dzialaczRynkowy);

                }

                automatyczneTworzenieInwestorowIFunduszy();
            }
        }).start();

    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }

    /**
     * tworzenie Gieldy , zwraca utworzon� gie�de
     *
     * @param nazwa nazwa tworzonej gie�dy
     * @param marza marza pobierana przy kazdej operacji kupna sprzedazy
     * @return utworzona gielda
     */
    public GieldaAkcji stworzGielde(String nazwa, Float marza) {
        int indx = random.nextInt(bazaMiejscGield.size());
        String[] miejsce = bazaMiejscGield.get(indx);
        GieldaAkcji nowaGielda = new GieldaAkcji(nazwa, miejsce[0].trim(), miejsce[1].trim(), marza);
        GieldaApp.GIELDY_AKCJI.add(nowaGielda);
        return nowaGielda;
    }

    /**
     * metoda zwraca utworzony obiekt Spolka
     *
     * @return obiekt typu Spolka
     */
    public Spolka stworzSpolke() {

        if (GieldaApp.GIELDY_AKCJI.size() > 0) {
            final int gieldaIdx = random.nextInt(GieldaApp.GIELDY_AKCJI.size());
            GieldaAkcji gieldaAkcji = GieldaApp.GIELDY_AKCJI.get(gieldaIdx);
            String nazwa = bazaNazwSpolek.get(0);
            bazaNazwSpolek.remove(nazwa);
            Float budzet = random.nextInt(900000) + 100000f;
            Float kurs = (random.nextFloat() + 1) * (random.nextInt(50) + 1);
            Spolka nowaSpolka = new Spolka(budzet, nazwa, kurs, gieldaAkcji);
            SpolkaThreadLogic spolkaThreadLogic = new SpolkaThreadLogic(nowaSpolka, 600);
            InfinitLoopThread infinitLoopThread = new InfinitLoopThread(spolkaThreadLogic);
            SPOLKI.put(nowaSpolka, infinitLoopThread);
            infinitLoopThread.start();
            return nowaSpolka;
        } else {
            return null;
        }
    }

    /**
     * metoda tworz�ca nowy w�tek Inwestora
     *
     * @return utworzony obiekt typu Inwestor
     */
    public Inwestor stworzInwestora() {
        String[] imieINazwisko = bazaImionINazwisk.get(0);
        bazaImionINazwisk.remove(imieINazwisko);
        String pesel = bazaNrPesel.get(0);
        Float budzet = random.nextInt(90000) + 10000f;
        Inwestor nowyInwestor = new Inwestor(imieINazwisko[0].trim(), imieINazwisko[1].trim(), pesel, budzet);
        InwestorThreadLogic inwestorThreadLogic = new InwestorThreadLogic(nowyInwestor, 600);
        InfinitLoopThread infinitLoopThread = new InfinitLoopThread(inwestorThreadLogic);
        INWESTORZY.put(nowyInwestor, infinitLoopThread);
        infinitLoopThread.start();
        OknoAplikacji.inwestorModel.addElement(nowyInwestor);
        return nowyInwestor;
    }

    /**
     * metoda tworz�ca nowy Fundusz
     *
     * @return utworzony obiekt typu Fundusz
     */
    public Fundusz stworzFundusz() {
        String[] imieINazwisko = bazaImionINazwisk.get(0);
        bazaImionINazwisk.remove(imieINazwisko);
        String nazwaFunduszu = bazaNazwFunduszy.get(0);
        bazaNazwFunduszy.remove(0);
        Float budzet = random.nextInt(9000) + 1000f;
        Float kurs = (random.nextFloat() + 1) * (random.nextInt(30) + 1);
        Fundusz nowyFundusz = new Fundusz(nazwaFunduszu, imieINazwisko[0].trim(), imieINazwisko[1].trim(), budzet, kurs, RYNEK_FUNDUSZY);
        RYNEK_FUNDUSZY.zarejestrujAktywoNaRynku(nowyFundusz);
        FunduszThreadLogic funduszThreadLogic = new FunduszThreadLogic(nowyFundusz, 600);
        InfinitLoopThread infinitLoopThread = new InfinitLoopThread(funduszThreadLogic);
        FUNDUSZE.put(nowyFundusz, infinitLoopThread);
        infinitLoopThread.start();
        OknoAplikacji.funduszModel.addElement(nowyFundusz);
        OknoAplikacji.aktywaModel.addElement(nowyFundusz);
        return nowyFundusz;
    }

    /**
     * tworzenie nowego surowca
     *
     * @return
     */
    public CenaSurowca stworzSurowiec() {
        String[] daneSurowca = bazaNazwSurowcow.get(0);
        bazaNazwSurowcow.remove(daneSurowca);
        Float kurs = (random.nextFloat() + 1) * (random.nextInt(30) + 1);
        CenaSurowca nowySurowiec = new CenaSurowca(daneSurowca[0].trim(), daneSurowca[1].trim(), kurs, RYNEK_SUROWCOW);
        RYNEK_SUROWCOW.zarejestrujAktywoNaRynku(nowySurowiec);
        return nowySurowiec;
    }

    /**
     * tworzenie nowej waluty
     *
     * @return
     */
    public KursWaluty stworzWalute() {
        String[] nazwaWaluty = bazaWalut.get(0);
        bazaWalut.remove(nazwaWaluty);
        Float kurs = (random.nextFloat() + 1) * (random.nextInt(30) + 1);
        KursWaluty nowaWaluta = new KursWaluty(nazwaWaluty[0].trim(), nazwaWaluty[1].trim(), kurs, RYNEK_WALUT);
        RYNEK_WALUT.zarejestrujAktywoNaRynku(nowaWaluta);
        return nowaWaluta;
    }

    /**
     * automatyczne tworzenie nowych w�tk�w inwestor�w i funduszy
     */
    public void automatyczneTworzenieInwestorowIFunduszy() {//jak tu bedzie jak antywaNaRynkuzwrocaNULL
        int iloscAktywNaWszystkichRynkach = 0;
        iloscAktywNaWszystkichRynkach += GIELDY_AKCJI.stream()
                .map(gielda -> gielda.aktywaNaRynku())
                .flatMap(aktywa -> aktywa.stream())
                .collect(Collectors.toList())
                .size();
        iloscAktywNaWszystkichRynkach += RYNEK_SUROWCOW.aktywaNaRynku().size();
        iloscAktywNaWszystkichRynkach += RYNEK_WALUT.aktywaNaRynku().size();
        int iloscNowych = (iloscAktywNaWszystkichRynkach / 3) - INWESTORZY.size() - FUNDUSZE.size();
        while (iloscNowych > 0) {
            Inwestor nowyInwestor = stworzInwestora();
            Fundusz nowyFundusz = stworzFundusz();
            iloscNowych -= 2;
        }

    }

    /**
     * serializacja aplikacji
     */
    public void serializacja() {
        zatrzymajProgram();
        try (final FileOutputStream file = new FileOutputStream("serializacja.txt")) {
            final ObjectOutputStream objectOutputStream = new ObjectOutputStream(file);
            objectOutputStream.writeObject(poczatekSymulacji);
            objectOutputStream.writeObject(GIELDY_AKCJI);
            objectOutputStream.writeObject(RYNEK_WALUT);
            objectOutputStream.writeObject(RYNEK_SUROWCOW);
            objectOutputStream.writeObject(RYNEK_FUNDUSZY);
            objectOutputStream.writeObject(INWESTORZY);
            objectOutputStream.writeObject(FUNDUSZE);
            objectOutputStream.writeObject(SPOLKI);
            objectOutputStream.writeObject(bazaImionINazwisk);
            objectOutputStream.writeObject(bazaNrPesel);
            objectOutputStream.writeObject(bazaNazwFunduszy);
            objectOutputStream.writeObject(bazaNazwSpolek);
            objectOutputStream.writeObject(bazaNazwSurowcow);
            objectOutputStream.writeObject(bazaMiejscGield);
            objectOutputStream.writeObject(bazaWalut);

        } catch (IOException e) {
            e.printStackTrace();
        }
        wznowProgram2();
    }

    /**
     * usuniecie dotychczasowego stanu aplikacji
     */
    public void usunProgram() {
        new HashMap<>(SPOLKI).forEach((k, v) -> k.usun());
        new HashMap<>(INWESTORZY).forEach((k, v) -> k.usun());
        new HashMap<>(FUNDUSZE).forEach((k, v) -> k.usun());
    }

    /**
     * chwilowe zatrzymanie aplikacji
     */
    public void zatrzymajProgram() {
        INWESTORZY.forEach((k, v) -> v.pauseRunning());
        FUNDUSZE.forEach((k, v) -> v.pauseRunning());
        SPOLKI.forEach((k, v) -> v.pauseRunning());
    }

    /**
     * wznowienie w�tk�w aplikacji po wywo�aniu metody zatrzymajProgram()
     */
    public void wznowProgram() {
        INWESTORZY.forEach((k, v) -> v.start());
        FUNDUSZE.forEach((k, v) -> v.start());
        SPOLKI.forEach((k, v) -> v.start());
    }

    /**
     * wy��czenie zatrzymania Aplikacji
     */
    public void wznowProgram2() {
        INWESTORZY.forEach((k, v) -> v.resumeRunning());
        FUNDUSZE.forEach((k, v) -> v.resumeRunning());
        SPOLKI.forEach((k, v) -> v.resumeRunning());
    }

    /**
     * deserializacja Aplikacji
     */
    public void deserializacja() {
        usunProgram();
        try (final FileInputStream file = new FileInputStream("serializacja.txt")) {
            final ObjectInputStream objectInputStream = new ObjectInputStream(file);
            poczatekSymulacji = (Long) objectInputStream.readObject();
            GIELDY_AKCJI = (List<GieldaAkcji>) objectInputStream.readObject();
            RYNEK_WALUT = (RynekWalut) objectInputStream.readObject();
            RYNEK_SUROWCOW = (RynekSurowcow) objectInputStream.readObject();
            RYNEK_FUNDUSZY = (RynekFunduszy) objectInputStream.readObject();
            INWESTORZY = (Map<Inwestor, InfinitLoopThread>) objectInputStream.readObject();
            FUNDUSZE = (Map<Fundusz, InfinitLoopThread>) objectInputStream.readObject();
            SPOLKI = (Map<Spolka, InfinitLoopThread>) objectInputStream.readObject();
            bazaImionINazwisk = (List<String[]>) objectInputStream.readObject();
            bazaNrPesel = (List<String>) objectInputStream.readObject();
            bazaNazwFunduszy = (List<String>) objectInputStream.readObject();
            bazaNazwSpolek = (List<String>) objectInputStream.readObject();
            bazaNazwSurowcow = (List<String[]>) objectInputStream.readObject();
            bazaMiejscGield = (List<String[]>) objectInputStream.readObject();
            bazaWalut = (List<String[]>) objectInputStream.readObject();
            wznowProgram();
            wznowProgram2();
            OknoAplikacji.odswiezOkno();
        } catch (IOException ex) {
            Logger.getLogger(GieldaApp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GieldaApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * wczytanie bazy nazw potrzebnych do tworzenia obiekt�w
     *
     * @throws IOException
     */
    public void wczytaj() throws IOException {
        Path path = Paths.get("imiona_i_nazwiska.csv");
        bazaImionINazwisk = java.nio.file.Files.lines(path)
                .map(s -> s.split(";"))
                .collect(Collectors.toList());
        Path path2 = Paths.get("bazaNrPesel.csv");
        bazaNrPesel = java.nio.file.Files.lines(path2)
                .filter(arr -> arr.length() == 11)
                .collect(Collectors.toList());

        Path path3 = Paths.get("bazaNazwFunduszy.csv");
        try {
            bazaNazwFunduszy = java.nio.file.Files.lines(path3, Charset.forName("UTF-8"))
                    .collect(Collectors.toList());
        } catch (Throwable t) {
            System.out.println(t.getLocalizedMessage());
        }
        Path path4 = Paths.get("bazaNazwSpolek.csv");
        bazaNazwSpolek = java.nio.file.Files.lines(path4, Charset.forName("UTF-8"))
                .collect(Collectors.toList());

        Path path5 = Paths.get("bazaSurowcow.csv");
        bazaNazwSurowcow = java.nio.file.Files.lines(path5, Charset.forName("UTF-8"))
                .map(s -> s.split(";"))
                .collect(Collectors.toList());

        Path path6 = Paths.get("bazaMiejscGield.csv");
        bazaMiejscGield = java.nio.file.Files.lines(path6, Charset.forName("UTF-8"))
                .map(s -> s.split(";"))
                .collect(Collectors.toList());
        Path path7 = Paths.get("bazaWalut.csv");
        bazaWalut = java.nio.file.Files.lines(path7, Charset.forName("UTF-8"))
                .map(s -> s.split(";"))
                .collect(Collectors.toList());

    }

}
